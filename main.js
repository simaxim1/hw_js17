
/*
Створити об'єкт "студент" та проаналізувати його табель. Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

#### Технічні вимоги:
- Створити порожній об'єкт `student`, з полями `name` та `lastName`.
- Запитати у користувача ім'я та прізвище студента, отримані значення записати у відповідні поля об'єкта.
- У циклі запитувати у користувача назву предмета та оцінку щодо нього. Якщо користувач натисне Cancel при n-питанні про назву предмета, закінчити цикл. Записати оцінки з усіх предметів в властивість об'єкт `student` - `tabel`. 
- порахувати кількість поганих (менше 4) оцінок з предметів. Якщо таких немає, вивести повідомлення "Студент переведено на наступний курс".
- Порахувати середній бал з предметів. Якщо він більше 7 – вивести повідомлення `Студенту призначено стипендію`. 
*/

const name = prompt('Add Your name');
const lastName = prompt('Add Your Last name');

let student = {
    name,
    lastName,

};

student.table = {};

while (true) {

    const subject = prompt('Add subject');

    if (subject === null) {
        break;
    }

    const grade = prompt('Add grade');
    
    if (grade === null) {
        break;
    }
    
    student.table[subject] = +grade;

};

const tableData = student.table;
let gradeSum = 0;
let gradeCount = 0;
let badGradeCounter = 0;

for(let key in tableData) {

    if (tableData.hasOwnProperty(key)) {
        gradeSum +=tableData[key];
        gradeCount++;

    };

    if (tableData[key] <4) {
        badGradeCounter++;
        
    }

};

if(badGradeCounter>0) {

    console.log(`Поганих оцінок ${badGradeCounter}, студента  ${student.name} ${student.lastName} не переведено.`);

}

let gradeAveage = (gradeSum / gradeCount).toFixed(2);

if (gradeAveage>7) {

    console.log(`Студенту ${student.name} ${student.lastName} призначено стипендію!`);

};

console.log(`Cума балів - ${gradeSum}\nКількість оцінок - ${gradeCount}\nКількість поганих оцінок - ${badGradeCounter}\nСередній бал - ${gradeAveage}`);